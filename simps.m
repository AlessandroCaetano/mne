## Copyright (C) 2016 Alessandro Caetano
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} simps (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Alessandro Caetano <alessandrocb@shinano>
## Created: 2016-11-30
function[integral] = simps(func, subdiv, a, b)
  h=(b-a)/subdiv;
  soma1=0;
  soma2=0;
  xi=a+h;
  for i=1:subdiv-1
    if rem(i,2)~=0
      soma1=soma1+func(xi);
      xi=xi+h;
    else
      soma2=soma2+func(xi);
      xi=xi+h;
    end
  end
  integral = (h/3)*(func(a)+4*soma1+2*soma2+func(b));
end
