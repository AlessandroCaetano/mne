## Copyright (C) 2016 Alessandro Caetano
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} solve_simps (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Alessandro Caetano <alessandrocb@shinano>
## Created: 2016-11-30

function[integral] = solve_simps(option)
  subdiv = input("Insira o numero de subsdivisoes\n");
  switch(option)
    case 1
      integral = simps(@f1, subdiv, 1 , 2);
    case 2
      integral = simps(@f2, subdiv, 1, 4);
    case 3
      integral = simps(@f3, subdiv, 2, 14);
    otherwise
      printf("Opcao invalida\n");
  endswitch
endfunction

