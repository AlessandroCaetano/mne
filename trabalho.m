option = input("Escolha o método de resoluçao das integrais \n 1 - Trapezio Repetido \n 2 - 1/3 de Simpson\n");

if(option == 1)
  printf("Trapezio Repetido\n");
  option_2 = input("Escolha a integral que deseja resolver:\n 1 - e^xdx\n 2 - sqrt(x)dx\n 3 - 1/sqrt(x)dx\n");
  integral = solve_trap(option_2);
  printf("%.10f\n", integral);
else 
  printf("1/3 de Simpson\n");
  option_2 = input("Escolha a integral que deseja resolver:\n 1 - e^xdx\n 2 - sqrt(x)dx\n 3 - 1/sqrt(x)dx\n");
  integral = solve_simps(option_2);
  printf("%.10f\n", integral);
end