## Copyright (C) 2016 Alessandro Caetano
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} trap (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Alessandro Caetano <alessandrocb@shinano>
## Created: 2016-11-30
function[integral] = trap(func, subdiv, a, b)
  h=(b-a)/subdiv;
  xi=a+h;
  soma=0;
  for i=1:subdiv-1
    soma=soma+func(xi);
    xi=xi+h;
  end
  integral = (h/2)*(func(a)+func(b))+h*soma;
endfunction